package linkedList;

class MyNode<T> {
    private T value;
    private MyNode<T> next;

    MyNode(T value) {
        this.value = value;
    }

    T getValue() {
        return value;
    }

    MyNode<T> getNext() {
        return next;
    }

    void setNext(MyNode<T> next) {
        this.next = next;
    }

    boolean hasNext() {
        return getNext() != null;
    }
}
