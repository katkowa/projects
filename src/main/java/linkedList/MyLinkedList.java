package linkedList;

public class MyLinkedList<T> {

    public static void main(String[] args) {
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(10);
        myLinkedList.add(1);
        myLinkedList.add(2);
        myLinkedList.add(3);

        System.out.println(myLinkedList.getFirstElement());
        System.out.println(myLinkedList.get(1));
        System.out.println(myLinkedList.get(2));
        System.out.println(myLinkedList.get(3));
        System.out.println("size: " + myLinkedList.size());

        System.out.println("------------------------");
        myLinkedList.addFirst(5);
        System.out.println(myLinkedList.getFirstElement());
        System.out.println(myLinkedList.get(1));
        System.out.println(myLinkedList.get(2));

        System.out.println(myLinkedList.toString());
        myLinkedList.delete(1);
        System.out.println(myLinkedList.toString());
        myLinkedList.addAfterIndex(100, 2);
        System.out.println(myLinkedList.toString());
    }

    private MyNode<T> head;
    private int size;

    public MyLinkedList() {
        head = null;
    }

    public MyLinkedList(T... values) {
        for (T value : values) {
            add(value);
        }
    }

    public void add(T value) {
        if (head == null) {
            head = new MyNode<>(value);
        } else {
            MyNode<T> lastNode = head;
            while (lastNode.hasNext()) {
                lastNode = lastNode.getNext();
            }
            MyNode<T> newNode = new MyNode<>(value);
            lastNode.setNext(newNode);
        }
        size++;
    }

    public void add(T... values) {
        for (T value : values) {
            add(value);
        }

    }

    public void addFirst(T value) {
        MyNode<T> newNode = new MyNode<>(value);
        newNode.setNext(head);
        head = newNode;
        size ++;
    }

    public void addAfterIndex(T value, int index) {
        MyNode<T> newNode = new MyNode<>(value);
        MyNode<T> beforeNode = getNode(index);
        newNode.setNext(beforeNode.getNext());
        beforeNode.setNext(newNode);
        size ++;
    }

    public T getFirstElement() {
        return head.getValue();
    }

    public T get(int index) {
        MyNode<T> node = getNode(index);
        return node.getValue();
    }

    private MyNode<T> getNode(int index) {
        MyNode<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.getNext();
        }
        return node;
    }

    public void delete(int index) {
        getNode(index - 1).setNext(getNode(index + 1));
        size --;
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("My Linked List: ");
        for (int i = 0; i < size; i++) {
            result.append(get(i)).append(" ");
        }
        return result.toString();
    }
}
