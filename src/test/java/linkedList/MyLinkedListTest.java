package linkedList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MyLinkedListTest {

    @Test
    public void shouldAddElementToEmptyList() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();

        // when
        myLinkedList.add(5);

        // then
        Assertions.assertEquals(5, (int) myLinkedList.getFirstElement());
        Assertions.assertEquals(1, myLinkedList.size());
    }

    @Test
    public void shouldAddNextElements() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();

        // when
        myLinkedList.add(1);
        myLinkedList.add(2);
        myLinkedList.add(3);

        // then
        Assertions.assertEquals(1, (int) myLinkedList.get(0));
        Assertions.assertEquals(2, (int) myLinkedList.get(1));
        Assertions.assertEquals(3, (int) myLinkedList.get(2));
        Assertions.assertEquals(3, myLinkedList.size());
    }

    @Test
    public void shouldAddFirstElement() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(0);
        myLinkedList.add(1);
        myLinkedList.add(2);

        // when
        myLinkedList.addFirst(10);
        Integer firstElement = myLinkedList.getFirstElement();

        // then
        Assertions.assertEquals(10, (int) firstElement);
        Assertions.assertEquals(4, myLinkedList.size());
    }

    @Test
    public void shouldAddElementAfterIndex() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(0);
        myLinkedList.add(1);
        myLinkedList.add(2);

        // when
        myLinkedList.addAfterIndex(10, 1);

        // then
        Assertions.assertEquals(10, (int) myLinkedList.get(2));
        Assertions.assertEquals(4, myLinkedList.size());
    }

    @Test
    public void shouldDeleteElement() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(0);
        myLinkedList.add(1);
        myLinkedList.add(2);

        // when
        myLinkedList.delete(1);

        // then
        Assertions.assertEquals(2, (int) myLinkedList.get(1));
        Assertions.assertEquals(2, myLinkedList.size());
    }

    @Test
    public void shouldGiveSizeOfEmptyList() {
        // given
        MyLinkedList myLinkedList = new MyLinkedList();

        // when
        int size = myLinkedList.size();

        // then
        Assertions.assertEquals(0, size);
    }

    @Test
    public void shouldGiveSize() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(0);
        myLinkedList.add(1);
        myLinkedList.add(2);

        // when
        int size = myLinkedList.size();

        // then
        Assertions.assertEquals(3, size);
    }

    @Test
    public void shouldGetFirstElement() {
        // given
        MyLinkedList<String> myLinkedList = new MyLinkedList<>();

        // when
        myLinkedList.add("Kasia");

        // then
        Assertions.assertEquals("Kasia", myLinkedList.getFirstElement());
    }

    @Test
    public void shouldGetAnyElement() {
        // given
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>();
        myLinkedList.add(0);
        myLinkedList.add(1);
        myLinkedList.add(2);
        myLinkedList.add(3);

        // when
        int secondElement = myLinkedList.get(1);
        int lastElement = myLinkedList.get(myLinkedList.size() - 1);

        // then
        Assertions.assertEquals(1, secondElement);
        Assertions.assertEquals(3, lastElement);
    }

    @Test
    public void shouldAddManyElementsWhileCreated() {
        // when
        MyLinkedList<Integer> myLinkedList = new MyLinkedList<>(1, 2, 3, 4, 5);

        // then
        Assertions.assertEquals(5, myLinkedList.size());
    }

    @Test
    public void shouldAddManyElements() {
        // given
        MyLinkedList<String> myLinkedList = new MyLinkedList<>();

        // when
        myLinkedList.add("word1", "word2", "word3");

        // then
        Assertions.assertEquals(3, myLinkedList.size());
        Assertions.assertEquals("word1", myLinkedList.getFirstElement());
        Assertions.assertEquals("word2", myLinkedList.get(1));
        Assertions.assertEquals("word3", myLinkedList.get(2));
    }


}