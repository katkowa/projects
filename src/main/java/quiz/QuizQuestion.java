package quiz;

import java.util.HashSet;
import java.util.Set;

public class QuizQuestion {
    private String value;
    private Set<Answer> answers = new HashSet<>();

    String getValue() {
        return value;
    }

    void setValue(String value) {
        this.value = value;
    }

    void addAnswer(String value, boolean isCorrect) {
        Answer answer = new Answer(value, isCorrect);
        answers.add(answer);
    }

    private String getAnswers() {
        StringBuilder builder = new StringBuilder();
        char id = 'A';
        for (Answer answer : answers) {
            answer.setId(id);
            builder.append(id).append(") ").append(answer.value).append("\n");
            id++;
        }
        String result = builder.toString();
        return result.substring(0, result.length() - 1);
    }

    @Override
    public String toString() {
        return value + "\n" + getAnswers();
    }

    Answer getAnswerFromId(char id) {
        for (Answer answer : answers) {
            if (answer.id == id) {
                return answer;
            }
        }
        throw new RuntimeException("There is no answer with this id");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuizQuestion that = (QuizQuestion) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        return answers != null ? answers.equals(that.answers) : that.answers == null;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (answers != null ? answers.hashCode() : 0);
        return result;
    }
}

class Answer {
    String value;
    boolean isCorrect;
    char id;

    public Answer(String value, boolean isCorrect) {
        this.value = value;
        this.isCorrect = isCorrect;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Answer answer = (Answer) o;

        if (isCorrect != answer.isCorrect) return false;
        return value != null ? value.equals(answer.value) : answer.value == null;
    }

    public void setId(char id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (isCorrect ? 1 : 0);
        return result;
    }
}
