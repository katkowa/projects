package sapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SapperSimulation extends Sapper {
    /**
     * choose random cell
     * while true:
     * 	moves = 0
     * 	for every cell:
     * 		mines -> number of mines beside
     * 		flags -> number of flag besides
     * 		empties -> number of empty cells besides
     * 		if mines == flags:
     * 			discovere empty cells
     * 			moves++
     * 		if mines - flags == empties:
     * 			empty = flag
     * 			moves++
     * 	if moves == 0:
     * 		choose random from empty cells
     */

    private static int winCounter = 0;
    private static int lostCounter = 0;

    public static void main(String[] args) {
        int n = 1;
        while (n > 0) {
            SapperSimulation simulation = new SapperSimulation(20, 20, 40);
            try {
                simulation.play();
            } catch (WinException e) {
                simulation.win(e.getMessege());
            } catch (LoseException e) {
                simulation.lost(e.getMessege());
            }
            n--;
        }
        System.out.printf("Win: %s, lost: %s%n", winCounter, lostCounter);
    }

    private void win(String messege) {
        System.out.println(messege);
        checkBoard("F");
        winCounter++;
    }

    private void lost(String messege) {
        System.out.println(messege);
        checkBoard("B");
        lostCounter++;
    }


    public SapperSimulation(int lines, int columns, int m) {
        super(lines, columns, m);
    }

    public void play() throws WinException, LoseException {
        Cell firstCell = getRandomCell();
        placeMines(firstCell.getLine(), firstCell.getColumn());
        singleMove(firstCell.getLine(), firstCell.getColumn());
        displayBoard();
        while (!isWon()) {
            int moves = 0;
            for (int i = 0; i < numberOfLines; i++) {
                for (int j = 0; j < numberOfColumns; j++) {
                    if (boardToShow[i][j] != null && !boardToShow[i][j].equals("\u2691")) {
                        int mines = countMines(i, j);
                        int empties = countEmptyCells(i, j);
                        int flags = countFlags(i, j);
                        if (mines == flags) {
                            for (Cell cell : getEmptyCells(i, j)) {
                                if (boardToShow[cell.getLine()][cell.getColumn()] == null) {
                                    moves++;
                                    singleMove(cell.getLine(), cell.getColumn());
                                }
                                displayBoard();
                                if (isWon()) {
                                    throw new WinException(movesCounter);
                                }
                            }
                        }
                        if (mines - flags == empties) {
                            for (Cell cell : getEmptyCells(i, j)) {
                                moves++;
                                placeFlag(cell.getLine(), cell.getColumn());
                                displayBoard();
                                if (isWon()) {
                                    throw new WinException(movesCounter);
                                }
                            }
                        }
                    }
                }
            }
            if (moves == 0) {
                Cell cell = getRandomCell();
                singleMove(cell.getLine(), cell.getColumn());
                //displayBoard();
                if (isWon()) {
                    throw new WinException(movesCounter);
                }
            }
        }
    }

    private int countEmptyCells(int l, int c) {
        int n = 0;
        for (int i = l-1; i <= l+1; i++) {
            if (i >= 0 && i < numberOfLines) {
                for (int j = c-1; j <= c+1; j++) {
                    if (j >= 0 && j < numberOfColumns) {
                        if (!(i == l && j == c)) {
                            if (boardToShow[i][j] == null) {
                                n++;
                            }
                        }
                    }
                }
            }
        }
        return n;
    }

    private int countFlags(int l, int c) {
        int n = 0;
        for (int i = l-1; i <= l+1; i++) {
            if (i >= 0 && i < numberOfLines) {
                for (int j = c-1; j <= c+1; j++) {
                    if (j >= 0 && j < numberOfColumns) {
                        if (!(i == l && j == c)) {
                            if (boardToShow[i][j] != null) {
                                if (boardToShow[i][j].equals("\u2691")) {
                                    n++;
                                }
                            }
                        }
                    }
                }
            }
        }
        return n;
    }

    private List<Cell> getEmptyCells(int l, int c) {
        List<Cell> result = new ArrayList<>();
        for (int i = l-1; i <= l+1; i++) {
            if (i >= 0 && i < numberOfLines) {
                for (int j = c-1; j <= c+1; j++) {
                    if (j >= 0 && j < numberOfColumns) {
                        if (!(i == l && j == c)) {
                            if (boardToShow[i][j] == null) {
                                result.add(new Cell(i, j));
                            }
                        }
                    }
                }
            }
        }
        return result;
    }


    private Cell getRandomCell() {
        Random random = new Random();
        int l;
        int c;
        while (true) {
            l = random.nextInt(numberOfLines);
            c = random.nextInt(numberOfColumns);
            if (boardToShow[l][c] == null) {
                return new Cell(l,c);
            }
        }
    }

    private boolean isWon() {
        return minesCounter == numberOfMines || emptyCounter == numberOfColumns * numberOfLines - numberOfMines;
    }
}

