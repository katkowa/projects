package currency;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Napisz program, który wyświetli aktualne kursy wszystkich dostępnych walut
 * i ile warte jest 100 pln w tych walutach
 * kursy należy pobrać z serwisu internetowego, a wyniki wyświetlić na konsoli
 * Zadanie dodatkowe:
 *     przy przeliczaniu 100 pln na waluty użyj kursu sprzedaży
 *     oblicz ile bysmy zarobili/stracili, zamieniając 100 pln na walutę miesiąc temu
 *     (tj. ile dziś warte (w pln) jest x eur, które kupiliśmy miesiąc temu za 100 pln)
 *
 * źródło danych: api.nbp.pl
 */
public class CurrencyConverter {
    public static void main(String[] args) {
//        Currencies currencies = getCurrencies(LocalDate.now());
//        System.out.println(currencies.rates.get(0).currency);

        CurrencyConverter converter = new CurrencyConverter(LocalDate.now());

        converter.showCurrencies();
        System.out.println("------------------------------------------------------------------------------");
        converter.showInvestmentResults();
    }

    Currencies currencies;

    public CurrencyConverter(LocalDate date) {
        currencies = getCurrencies(date);
    }

    public static Currencies getCurrencies(LocalDate date) {
        Gson gson = new Gson();
        URLConnection connection = null;
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/tables/c/" + date.toString() + "/?format=json");
            connection = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (InputStreamReader reader = new InputStreamReader(connection.getInputStream())){
            JsonArray jsonArray = gson.fromJson(reader, JsonArray.class);
            JsonElement jsonElement = jsonArray.get(0);
            Currencies currencies = gson.fromJson(jsonElement, Currencies.class);
            return currencies;
        } catch (Exception e) {
            return getCurrencies(date.minusDays(1));
        }
    }

    public Rate getCurrency(CurrencyCode currencyCode) {
        for (Rate currency: currencies.rates) {
            if (currency.code.equals(currencyCode.getCode())) {
                return currency;
            }
        }
        return null;
    }

    public void showCurrencies() {
        System.out.println("Currency rates for " + currencies.effectiveDate + ":");
        currencies.rates.forEach(c -> System.out.printf("%s, 100PLN = %.2f%s%n", c, c.convertToCurrency(100.0), c.code));
    }

    public void showInvestmentResults()  {
        Currencies currenciesMonthAgo = getCurrencies(LocalDate.now().minusMonths(1));
        System.out.println("The investment result of exchanging 100PLN to other currencies from " + currenciesMonthAgo.effectiveDate + " to " + currencies.effectiveDate + ":");
        for (int i = 0; i < currencies.rates.size(); i++) {
            Rate rateNow = currencies.rates.get(i);
            Rate rateMonthAgo = currenciesMonthAgo.rates.get(i);
            Double convertToCurrency = rateMonthAgo.convertToCurrency(100.0);
            Double convertToPln = rateNow.convertToPln(convertToCurrency);
            Double result = convertToPln - 100;
            System.out.printf("%s: %.2fPLN%n", rateNow.currency, result);
        }
    }
}

class Currencies {
    String effectiveDate;
    ArrayList<Rate> rates;
}

class Rate {
    String currency;
    String code;
    private String bid;
    private String ask;

    double getBid() {
        return Double.valueOf(bid);
    }

    double getAsk() {
        return Double.valueOf(ask);
    }

    double convertToCurrency(Double sum) {
        return sum / getAsk();
    }

    double convertToPln(Double sum) {
        return sum * getBid();
    }

    @Override
    public String toString() {
        return currency + ": " + getBid() + "/" + getAsk();
    }
}
