package currency;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Napisz program, który wyświetli aktualne kursy średnie czterech wybranych walut (USD, EUR, GBP, CHF)
 * i ile warte jest 100 pln w tych walutach
 * kursy należy pobrać z serwisu internetowego, a wyniki wyświetlić na konsoli
 * Zadanie dodatkowe:
 *     przy przeliczaniu 100 pln na waluty użyj kursu sprzedaży
 *     oblicz ile bysmy zarobili/stracili, zamieniając 100 pln na walutę miesiąc temu
 *     (tj. ile dziś warte (w pln) jest x eur, które kupiliśmy miesiąc temu za 100 pln)
 *
 * źródło danych: api.nbp.pl
 */

public class SingleCurrencyConverter {
    public static void main(String[] args){
        SingleCurrencyConverter converter = new SingleCurrencyConverter();
        converter.showCurrencyRates(CurrencyCode.USD, CurrencyCode.EUR, CurrencyCode.GBP, CurrencyCode.CHF);

        System.out.println("-------------------------------------------------------------------------");

        Double investmentUSD = converter.calculateInvestment(CurrencyCode.USD);
        System.out.printf("The investment result of exchanging 100PLN to USD: %.2fPLN%n", investmentUSD);
        Double investmentEUR = converter.calculateInvestment(CurrencyCode.EUR);
        System.out.printf("The investment result of exchanging 100PLN to EUR: %.2fPLN%n", investmentEUR);
    }

    private static final Gson gson = new Gson();

    public double calculateInvestment(CurrencyCode currencyCode)  {
        Currency currencyToday = getCurrency(currencyCode, LocalDate.now());
        Currency currencyMonthAgo = getCurrency(currencyCode, LocalDate.now().minusMonths(1));
        double currencyBoughtMonthAgo = currencyMonthAgo.convertToCurrency(100.0);
        double plnBoughtToday = currencyToday.convertToPln(currencyBoughtMonthAgo);
        return plnBoughtToday - 100;
    }

    public void showCurrencyRates(CurrencyCode... currencyCodes) {
        List<Currency> currencies = getCurrencies(currencyCodes);
        System.out.println("Currency rates for " + currencies.get(0).getEffectiveDate() + ":");
        currencies.forEach(currency -> {
            double valueInPln = currency.convertToCurrency(100.0);
            System.out.printf("%s, 100PLN = %.2f%s%n", currency, valueInPln, currency.getCode());
        });
    }

    public List<Currency> getCurrencies(CurrencyCode[] currencyCodes) {
        List<Currency> currencies = new ArrayList<>(currencyCodes.length);
        for (CurrencyCode currencyCode : currencyCodes) {
            Currency currency = getCurrency(currencyCode, LocalDate.now());
            currencies.add(currency);
        }
        return currencies;
    }

    public Currency getCurrency(CurrencyCode currencyCode, LocalDate date) {
        URLConnection connection = null;
        try {
            URL url = new URL("http://api.nbp.pl/api/exchangerates/rates/c/" + currencyCode + "/" + date.toString() + "/?format=json");
            connection = url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (InputStreamReader reader = new InputStreamReader(connection.getInputStream())){
            Currency currency = gson.fromJson(reader, Currency.class);
            reader.close();
            return currency;
        } catch (Exception e) {
            return getCurrency(currencyCode, date.minusDays(1));
        }
    }
}

class Currency {
    private String currency;
    private String code;
    private List<Rate2> rates;

    public String getCode() {
        return code;
    }

    public double getBid() {
        return rates.get(0).getBid();
    }

    public double getAsk() {
        return rates.get(0).getAsk();
    }

    public String getEffectiveDate() {
        return rates.get(0).getEffectiveDate();
    }

    public double convertToCurrency(Double sum) {
        return sum / getAsk();
    }

    public double convertToPln(Double sum) {
        return sum * getBid();
    }

    @Override
    public String toString() {
        return currency + ": " + getBid() + "/" + getAsk();
    }
}

class Rate2 {
    private String effectiveDate;
    private double bid; // selling the currency
    private double ask; // buying the currency

    double getBid() {
        return bid;
    }

    double getAsk() {
        return ask;
    }

    String getEffectiveDate() {
        return effectiveDate;
    }
}

