import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.Set;

public class ChuckNorris {
    public static void main(String[] args) throws IOException {
        Set<Joke> jokes = getJokes(10);
        jokes.forEach(joke -> System.out.println(joke.getValue()));
    }

    private static Set<Joke> getJokes(int numberOfJokes) throws IOException {
        Set<Joke> jokes = new HashSet<>();
        while (jokes.size() < numberOfJokes) {
            jokes.add(getJoke());
        }
        return jokes;
    }

     static Joke getJoke() throws IOException {
        Gson gson = new Gson();
        URL url = new URL("https://api.chucknorris.io/jokes/random");
        URLConnection connection = url.openConnection();
        connection.addRequestProperty("User-Agent", "Mozilla");
        InputStreamReader reader = new InputStreamReader(connection.getInputStream());
        Joke joke = gson.fromJson(reader, Joke.class);
        reader.close();
        return joke;
    }

    public class Joke {
        private String id;
        private String value;

        public String getValue() {
            return value;
        }

        public Joke(String id, String value) {
            this.id = id;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Joke joke = (Joke) o;

            return id != null ? id.equals(joke.id) : joke.id == null;
        }

        @Override
        public int hashCode() {
            return id != null ? id.hashCode() : 0;
        }
    }

}
