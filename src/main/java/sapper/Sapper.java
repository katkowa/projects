package sapper;

import java.util.Random;
import java.util.Scanner;

public class Sapper {
    public static void main(String[] args) {
//        System.out.println( "\uD83D\uDCA3"); // mine
//        System.out.println( "\u2691"); // flag

        Sapper sapper = new Sapper(8,8,10);
        try {
            sapper.play();
        } catch (LoseException e) {
            System.out.println(e.getMessege());
            sapper.checkBoard("B");
        } catch (WinException e) {
            System.out.println(e.getMessege());
            sapper.checkBoard("F");
        }

    }

    protected int numberOfColumns;
    protected int numberOfLines;
    protected int numberOfMines;
    protected int[][] board;
    protected String[][] boardToShow;
    protected int minesCounter = 0; // to determine if every mine was found
    protected int emptyCounter = 0; // to determine if every empty box was found
    protected int movesCounter = 0;
    // -1 -> mine
    // in others number of mines beside

    public Sapper(int lines, int columns, int m) {
        this.numberOfLines = lines;
        this.numberOfColumns = columns;
        this.numberOfMines = m;
        this.board = new int[lines][columns];
        this.boardToShow = new String[lines][columns];
    }

    // only to check if location and counting the mines is correct
    protected void checkBoard(String symbol) {
        System.out.println("---------------------------------------------------------------------------");
        for (int[] column : board) {
            for (int element : column) {
                if (element == -1) {
                    System.out.printf("[%s]", symbol);
                } else {
                    System.out.printf("[%s]", element);
                }
            }
            System.out.printf("%n");
        }
        System.out.println("---------------------------------------------------------------------------");
    }

    protected void displayBoard() {
        System.out.println("---------------------------------------------------------------------------");
        for (String[] column : boardToShow) {
            for (String line : column) {
                if (line == null) {
                    line = " ";
                }
                System.out.printf("[%s]", line);
            }
            System.out.printf("%n");
        }
        System.out.println("---------------------------------------------------------------------------");
    }

    protected void placeMines(int x, int y) {
        Random random = new Random();
        int n = numberOfMines;
        int l;
        int c;
        while (n > 0) {
            l = random.nextInt(numberOfLines);
            c = random.nextInt(numberOfColumns);
            if (canPutMine(l, c)) {
                if (l != x && c != y) {
                    board[l][c] = -1; // symbol of mine
                    n--;
                }
            }
        }
        countMines();
    }

    private boolean canPutMine(int l, int c) {
        if (board[l][c] == -1) {
            return false;
        }
        if (isMineSurrounded(l, c)) {
            return false;
        }
        // check if placing mine here doesn't make placement of already putted mine incorrect
        for (int i = l-1; i <= l+1; i++) {
            if (i >= 0 && i < numberOfLines) {
                for (int j = c-1; j <= c+1; j++) {
                    if (j >= 0 && j < numberOfColumns) {
                        if (!(i == l && j == c)) {
                            board[l][c] = -1;
                            if (isMineSurrounded(i, j)) {
                                board[l][c] = 0;
                                return false;
                            }
                            board[l][c] = 0;
                        }
                    }
                }
            }
        }
        return true;
    }

    // check if place to put mine have at least one place without mine besides
    // return false if it's true
    private boolean isMineSurrounded(int l, int c) {
        if (countMines(l, c) == 8) {
            return true;
        }
        if ((l == 0 || l == numberOfLines-1) && (c == 0 || c == numberOfColumns-1)) {
            if (countMines(l, c) == 3) {
                return true;
            }
        }
        if (l == 0 || l == numberOfLines-1 || c == 0 || c == numberOfColumns-1) {
            if (countMines(l, c) == 5) {
                return true;
            }
        }
        return false;
    }

    protected void countMines() {
        for (int l = 0; l < numberOfLines; l++) {
            for (int c = 0; c < numberOfColumns; c++) {
                if (board[l][c] != -1) {
                    board[l][c] = countMines(l, c);
                }
            }
        }
    }

    protected int countMines(int l, int c) {
        int n = 0;
        for (int i = l-1; i <= l+1; i++) {
            if (i >= 0 && i < numberOfLines) {
                for (int j = c-1; j <= c+1; j++) {
                    if (j >= 0 && j < numberOfColumns) {
                        if (!(i == l && j == c)) {
                            if (board[i][j] == -1) {
                                n++;
                            }
                        }
                    }
                }
            }
        }
        return n;
    }

    protected void placeFlag(int l, int c) {
        movesCounter++;
        if (boardToShow[l][c] == null) {
            boardToShow[l][c] = "F";
            if (board[l][c] == -1) {
                minesCounter++;
            }
        }
    }

    protected void singleMove(int l, int c) throws RuntimeException, LoseException {
        movesCounter++;
        if (boardToShow[l][c] != null && boardToShow[l][c] != "F") {
            throw  new IllegalArgumentException("Discovered already");
        }
        if (board[l][c] >= 0) {
            boardToShow[l][c] = String.valueOf(board[l][c]);
            emptyCounter++;
            showEmpty(l,c);
        }
        if (board[l][c] == -1) {
            boardToShow[l][c] = "B";
            throw new LoseException(movesCounter);
        }
    }

    private void showEmpty(int l, int c) {
        if (board[l][c] == 0) {
            for (int i = l-1; i <= l+1; i++) {
                if (i >= 0 && i < numberOfLines) {
                    for (int j = c-1; j <= c+1; j++) {
                        if (j >= 0 && j < numberOfColumns) {
                            if (i != l || j != c) {
                                if (boardToShow[i][j] == null) {
                                    boardToShow[i][j] = String.valueOf(board[i][j]);
                                    showEmpty(i, j);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void play() throws LoseException, WinException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to Saper! Try not to explode! ;)");
        displayBoard();
        System.out.println("Where do you wanna start? (line, column)");
        String[] place = scanner.nextLine().split(",");
        int l = Integer.parseInt(place[0].trim());
        int c = Integer.parseInt(place[1].trim());
        placeMines(l, c);
        singleMove(l, c);
        while(true) {
            displayBoard();
            System.out.println("What dou you want to do now? f - place flag, d - discover, e - exit");
            String answer = scanner.nextLine();
            if (answer.equals("e")) {
                break;
            }
            if (answer.equals("f") || answer.equals("d")) {
                try {
                    System.out.println("Where? (line, column)");
                    place = scanner.nextLine().split(",");
                    l = Integer.parseInt(place[0].trim());
                    c = Integer.parseInt(place[1].trim());
                    if (answer.equals("f")) {
                        placeFlag(l, c);
                    } else if (answer.equals("d")) {
                        try {
                            singleMove(l, c);
                        } catch (IllegalArgumentException iae) {
                            System.out.println(iae.getMessage());
                        }
                    }
                    if (minesCounter == numberOfMines || emptyCounter == numberOfColumns * numberOfLines - numberOfMines) {
                        throw new WinException();
                    }
                } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException iae) {
                    System.out.println("Invalid input");
                }
            } else {
                System.out.println("Invalid input");
            }
        }
    }

}
