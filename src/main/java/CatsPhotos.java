

import com.google.gson.Gson;
import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackAttachment;
import net.gpedro.integrations.slack.SlackMessage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Napisz program, który z Internetu pobierze 10 losowych zdjęć kotów, a na konsoli wyświetli informacje o każdym z pobranych obrazków (rozdzielczość i rozmiar pliku).
 * Pliki nazwij kot_01 ... kot_10 (zachowaj rozszerzenie, bo są też gify)
 * żródło: https://aws.random.cat/meow
 */
public class CatsPhotos {
    public static void main(String[] args) {
        CatsPhotos cats = new CatsPhotos(10);

        cats.saveCats();
        cats.showProperties();

//        cats.sendCatToSlack();
//
    }

    private int numberOfCats;
    private List<Cat> listOfCats = new ArrayList<>();

    public CatsPhotos(int numberOfCats) {
        this.numberOfCats = numberOfCats;
    }

    public static final Gson gson = new Gson();

    public Cat getCat() {
        try {
            URL url = new URL("https://aws.random.cat/meow");
            URLConnection connection = url.openConnection();
            InputStreamReader reader = new InputStreamReader(connection.getInputStream());
            Cat cat = gson.fromJson(reader, Cat.class);
            BufferedImage image = ImageIO.read(new URL(cat.file));
            reader.close();
            cat.image = image;
            return cat;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveCats() {
        Path basePath = Paths.get("src\\main\\resources\\cats");
        try {
            if (Files.exists(basePath)) {
                delete(basePath);
                Files.delete(basePath);
            }
            Files.createDirectory(basePath);
            ExecutorService threadPool = Executors.newFixedThreadPool(8);
            int counter = 1;
            while (counter <= numberOfCats) {
                int id = counter;
                threadPool.submit(() -> saveCat(id));
                counter++;
            }
            threadPool.shutdown();
            threadPool.awaitTermination(5, TimeUnit.MINUTES);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private int saveCat(int counter) throws IOException {
        Cat cat = getCat();
        listOfCats.add(cat);
        String extension = com.google.common.io.Files.getFileExtension(cat.file);
        String name = "kot_" + String.format("%02d", counter);
        cat.name = name;
        File catFile = new File("src\\main\\resources\\cats\\" + name + "." + extension);
        InputStream in = new URL(cat.file).openStream();
        Files.copy(in, catFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        cat.size = catFile.length();
//                ImageIO.write(cat.image, extension, catFile);
        return counter;
    }

    public void showProperties() {
        listOfCats.forEach(System.out::println);
    }

    private static void delete(Path pathToDelete) {
        // iterate elements in catalog to delete files
        // recursive removal of internal catalogs
        try (Stream<Path> list = Files.list(pathToDelete)) {
            List<Path> paths = list.collect(Collectors.toList());
            for (Path path : paths) {
                if (Files.isRegularFile(path)) {
                    Files.delete(path);
                } else if (Files.isDirectory(path)) {
                    delete(path);
                    Files.delete(path);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendToSlack(String message) {
        SlackApi slackApi = new SlackApi("https://hooks.slack.com/services/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");

        SlackMessage m = new SlackMessage(message);
        slackApi.call(m);
    }

    public void sendCatToSlack() throws IOException {
        SlackApi slackApi = new SlackApi("https://hooks.slack.com/services/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");
        Cat cat = getCat();
        SlackMessage slackMessage = new SlackMessage(ChuckNorris.getJoke().getValue());
        slackMessage.addAttachments(new SlackAttachment("zdjęcie kotełka").setImageUrl(cat.file));
        slackApi.call(slackMessage);
    }

    class Cat {
        String file;
        BufferedImage image;
        transient String name;
        long size;

        @Override
        public String toString() {
                        return name + " - resolution: " + image.getWidth() + " x " + image.getHeight() + ", size: " + size + " KB";
        }
    }
}
