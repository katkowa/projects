package sapper;

public class LoseException extends Exception {
    int numberOfMoves;

    public LoseException() {
    }

    public LoseException(int numberOfMoves) {
        this.numberOfMoves = numberOfMoves;
    }

    public String getMessege() {
        if (numberOfMoves == 0) {
            return "Booom! End of the game";
        } else {
            return "Boom! End of game after " + numberOfMoves + " moves.";
        }
    }

}
