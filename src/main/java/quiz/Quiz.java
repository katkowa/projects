package quiz;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Napisz program, który umożliwi użytkownikom grę w quiz
 * - interakcja z programem odbywa się za pomocą konsoli (System.in)
 * - wczytaj pytania z plików
 * - pozwól użytkownikowi wybrać kategorię pytań
 * - losuj pytanie i przemieszaj odpowiedzi
 * - po 10 pytaniach wyswietl użytkownikowi wynik końcowy i zakończ grę
 *
 * Pytania w pliku mają następującą strukturę:
 * - pierwsza linia: treść pytania
 * - druga linia: liczba wszystkich odpowiedzi
 * - trzecia linia: poprawna odpowiedź
 * - kolejne linie: błędne odpowiedzi (każda w osobnej linii)
 */
public class Quiz {
    public static void main(String[] args) {
        Quiz quiz = new Quiz(10);
        quiz.play();
    }

    private LinkedHashMap<String, ArrayList<QuizQuestion>> questions = new LinkedHashMap<>(); // category -> List of questions
    private int numberOfQuestions;
    private int points = 0;
    private Random random = new Random();
    private Scanner scanner = new Scanner(System.in);


    public Quiz(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
        getQuestions();
    }

    private ArrayList<QuizQuestion> getQuestionsFromFile(Path path) {
        ArrayList<QuizQuestion> questions = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(path);
            while (scanner.hasNextLine()) {
                QuizQuestion question = new QuizQuestion();
                question.setValue(scanner.nextLine());
                int numberOfAnswers = Integer.parseInt(scanner.nextLine());
                question.addAnswer(scanner.nextLine(), true);
                while (numberOfAnswers > 1) {
                    question.addAnswer(scanner.nextLine(), false);
                    numberOfAnswers--;
                }
                questions.add(question);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File with questions not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return questions;
    }

    private void getQuestions() {
        String folderPath = "src\\main\\resources\\quiz";
        try (Stream<Path> pathList = Files.list(Paths.get(folderPath))) {
            List<Path> paths = pathList.collect(Collectors.toList());
            for (Path path : paths) {
                ArrayList<QuizQuestion> questionsList = getQuestionsFromFile(path);
                String categoryName = path.getFileName().toString().replace(".txt", "").replaceAll("[_]+"," ");
                questions.put(categoryName, questionsList);
            }
            ArrayList<QuizQuestion> allQuestions = new ArrayList<>();
            questions.forEach((key, list) -> allQuestions.addAll(list));
            questions.put("All", allQuestions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private QuizQuestion getRandomQuestionFromCategory(String category) {
        ArrayList<QuizQuestion> questionsList = questions.get(category);
        int randomNumber = random.nextInt(questionsList.size() - 1);
        return questionsList.get(randomNumber);
    }

    private HashSet<QuizQuestion> getRandomQuestions(String category) {
        HashSet<QuizQuestion> questionsList = new HashSet<>(numberOfQuestions);
        while (questionsList.size() < numberOfQuestions) {
            QuizQuestion quizQuestion = getRandomQuestionFromCategory(category);
            questionsList.add(quizQuestion);
        }
        return questionsList;
    }

    private ArrayList<String> getAndDisplayCategories() {
        ArrayList<String> categories = new ArrayList<>();
        categories.addAll(questions.keySet());
        int id = 1;
        for (String category : categories) {
            System.out.println(id + ". " + category);
            id++;
        }
        return categories;
    }

    private String getCategory() {
        ArrayList<String> categories = getAndDisplayCategories();
        System.out.println("Please choose category by typing number: ");
        try {
            int categoryNumber = scanner.nextInt();
            String category = categories.get(categoryNumber - 1);
            System.out.println("Selected category : " + category);
            return category;
        } catch (Exception e) {
            System.out.println("There is no category with that number. Try again!");
            scanner.nextLine();
            return getCategory();
        }
    }

    private boolean singleQuestion(QuizQuestion question) {
        boolean isCorrect = false;
        try {
            System.out.println(question);
            System.out.println("What's your answer?");
                String userAnswer = scanner.nextLine().toUpperCase();
                System.out.print("Your answer: " + userAnswer + ". ");
                Answer answer = question.getAnswerFromId(userAnswer.charAt(0));
                isCorrect = answer.isCorrect;
        } catch (RuntimeException e) {
            System.out.println("There is no such answer. Try again!");
            System.out.println("---------------------------------------------------------------------------------------");
            singleQuestion(question);
        }
        return isCorrect;
    }

    private boolean singleQuestionWithTimer(QuizQuestion question) {
        boolean isCorrect = false;
        System.out.println(question);
        System.out.println("What's your answer?");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

        FutureTask<String> task = new FutureTask<>(() -> {
            String input = "";
            while ("".equals(input)) {
                try {
                    while (!bufferedReader.ready()) {
                        Thread.sleep(100);
                    }
                    input = bufferedReader.readLine();
                } catch (InterruptedException e) {
                    return null;
                }
            }
            return input.toUpperCase();
        });
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();

        try {
            String userAnswer = task.get(5, TimeUnit.SECONDS);
            System.out.print("Your answer: " + userAnswer + ". ");
            Answer answer = question.getAnswerFromId(userAnswer.charAt(0));
            isCorrect = answer.isCorrect;
            if (isCorrect) {
                System.out.println("That's correct! :)");
                points++;
            } else {
                System.out.println("Wrong answer! :<");
            }
        } catch (RuntimeException e) {
            System.out.println("There is no such answer. Try again!");
            System.out.println("---------------------------------------------------------------------------------------");
            singleQuestionWithTimer(question);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.out.println("You're out of time, sorry!");
        } finally {
            thread.interrupt();
        }
        return isCorrect;
    }

    public void play() {
        System.out.println("Hello! You think you are smart? Let's check it! :)" + "\n");
        String category = getCategory();
        HashSet<QuizQuestion> questions = getRandomQuestions(category);
        System.out.println("To answer the question enter: A, B ... You have only 5 sec to do that so hurry up!");
        System.out.println("Press ENTER to begin this adventure :)");
        scanner.nextLine();
        scanner.nextLine();
        System.out.println("---------------------------------------------------------------------------------------");
        System.out.println("Let's the show begin!!!");
        System.out.println("---------------------------------------------------------------------------------------");
        for (QuizQuestion question : questions) {
            singleQuestionWithTimer(question);
            System.out.println("Press ENTER to continue whenever you're ready.");
            System.out.println("---------------------------------------------------------------------------------------");

            scanner.nextLine();
        }
        System.out.println("End of game. Your scores: " + points + "/" + numberOfQuestions);
        if (points <= 5) {
            System.out.println("Could be better. But don't give up and try again! :D");
        } else {
            System.out.println("You are a genius! Congratulations!!! ^^");
        }
    }
}
