package sapper;

public class WinException extends Exception {
    int numberOfMoves;

    public WinException() {
    }

    public WinException(int numberOfMoves) {
        this.numberOfMoves = numberOfMoves;
    }

    public String getMessege() {
        if (numberOfMoves != 0) {
            return "You survived. Congratulations!!! It takes you " + numberOfMoves + " moves";
        } else {
            return "You survived. Congratulations!!!";
        }
    }
}
